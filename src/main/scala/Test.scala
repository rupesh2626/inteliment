import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.sql.SQLContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.hive.HiveContext

object Test {
  
      case class Orders(order_id:Int,order_date:String,order_customer_id:Int,order_status:String)
      case class Products(product_id:Int,product_category_id:Int,product_name:String,product_description:String,product_price:Float,product_image:String)
      case class Order_items(order_item_id:Int,order_item_order_id:Int,order_item_product_id:Int,order_item_quantity:Int,order_item_subtotal:Float,order_item_product_price:Float)
      case class Customers(customer_id:Int,customer_fname:String,customer_lname:String,customer_email:String,customer_password:String,customer_street:String,customer_city:String,customer_state:String,customer_zipcode:String)
     
     def main(args: Array[String]): Unit = {
     val conf = new SparkConf()
     conf.setMaster("local[*]")
     conf.setAppName("test")
     conf.set("spark.sql.shuffle.partitions","2")
     val sc = new SparkContext(conf)
     val sqlContext= new HiveContext(sc)
     // total sales for each day
     import sqlContext.implicits._
      val datasource = args(0)
    //creating data
      val customers = sc.textFile(datasource+"Customers.txt").map(customer=>{
      val spl = customer.split(";")
      Customers(spl(0).toInt,spl(1),spl(2),spl(3),spl(4),spl(5),spl(6),spl(7),spl(8))
    }).toDF.registerTempTable("customers")
    
    val products =sc.textFile(datasource+"Products.txt").map(product=>{
      val spl = product.split(";")
      Products(spl(0).toInt,spl(1).toInt,spl(2),spl(3),spl(4).toFloat,spl(5))
    }).toDF.registerTempTable("products")
    val order_items =  sc.textFile(datasource+"Order_Items.txt").map(order_item=>{
      val spl = order_item.split(",")
      Order_items(spl(0).toInt,spl(1).toInt,spl(2).toInt,spl(3).toInt,spl(4).toFloat,spl(5).toFloat)
    }).toDF.registerTempTable("order_items")
    val orders = sc.textFile(datasource+"Orders.txt").filter(order=>order.endsWith("CLOSED")||order.endsWith("COMPLETE")).map(order=>{
      val spl = order.split(",")
      Orders(spl(0).toInt,spl(1),spl(2).toInt,spl(3))
    }).toDF.registerTempTable("orders")
    
val q1="select order_date,sum(order_item_subtotal) total from orders o join order_items oi on o.order_id=oi.order_item_order_id group by order_date"

// total sales for each month
val q2="select date_format(order_date,'YYYYMM') as month,sum(order_item_subtotal) total from orders o join order_items oi on o.order_id=oi.order_item_order_id group by date_format(order_date,'YYYYMM')"

//avg sales for each date_format
val q3="select avg(total) avgsales from (select order_date,sum(order_item_subtotal) total from orders o join order_items oi on o.order_id=oi.order_item_order_id group by order_date) t"

// avg sales for each month
val q4="select avg(total) avgsales from (select date_format(order_date,'YYYYMM') as month,sum(order_item_subtotal) total from orders o join order_items oi on o.order_id=oi.order_item_order_id group by date_format(order_date,'YYYYMM'))t"
//name of the month having higheest sale
val q5="select month,total from (select month,total,rank() over (order by total desc) rank from (select date_format(order_date,'MMMM') month,sum(order_item_subtotal) total from orders o join order_items oi on o.order_id=oi.order_item_order_id group by date_format(order_date,'MMMM')) temp)temp2 where rank =1"

// top 10 revenue generating products
val q6="select order_item_product_id from (select order_item_product_id,rank() over (order by sum(order_item_subtotal) desc) rank from order_items group by order_item_product_id) t where rank <= 10 order by rank"

// top 3 purchased customers
val q7="select order_date,order_customer_id,total,rank from (select order_date,order_customer_id,total,rank() over (partition by order_date order by total desc) rank from (select order_date,order_customer_id,sum(order_item_subtotal) total from orders o join order_items oi on o.order_id=oi.order_item_order_id group by order_date,order_customer_id) t) temp where rank<=3 order by order_date"
// most sold products for each day per month
val q8="select order_date,order_item_product_id from (select order_date,order_item_product_id,rank() over (partition by order_date order by total desc) rank from (select order_date,order_item_product_id,sum(order_item_subtotal) total from orders r join order_items oi on r.order_id=oi.order_item_order_id group by order_date,order_item_product_id) t1) t2 where rank=1 order by order_date"

//count of distinct customer by state
val q9="select customer_state,count(DISTINCT customer_id) countofcust from customers group by customer_state"

//most popular category id
val q10="select product_category_id from (select product_category_id,countofpurchase,rank() over (order by countofpurchase desc) as rnk from (select product_category_id,count(product_id) countofpurchase from products join order_items on products.product_id=order_items.order_item_product_id group by product_category_id) t) t2 where rnk=1"
val queries = Array(q1,q2,q3,q4,q5,q6,q7,q8,q9,q10)
for (i <- queries){
  sqlContext.sql(i).show
}
  sc.stop()   
    }
}